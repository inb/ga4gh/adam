/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.adam.model;

import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

public class Header {
    
    private String matrixName;
    private String matrixVersion;
    private List<String> matrixReferences;
    private String matrixProfileCreateDate;
    private List<ProfileUpdates> matrixProfileUpdates;
    private String resourceName;
    private List<String> resourceReferences;
    private String resourceDescription;
    private DataLevel resourceDataLevel;
    private List<Contact> resourceContactNames;
    private List<String> resourceContactOrganisations;
    
    public String getMatrixName() {
        return matrixName;
    }

    public void setMatrixName(final String matrixName) {
        this.matrixName = matrixName;
    }
    
    public String getMatrixVersion() {
        return matrixVersion;
    }

    public void setMatrixVersion(final String matrixVersion) {
        this.matrixVersion = matrixVersion;
    }

    public List<String> getMatrixReferences() {
        return matrixReferences;
    }

    public void setMatrixReferences(final List<String> matrixReferences) {
        this.matrixReferences = matrixReferences;
    }
    
    public void setMatrixProfileCreateDate(final String date) {
        this.matrixProfileCreateDate = date;
    }
    
    public String getMatrixProfileCreateDate() {
        return matrixProfileCreateDate;
    }
    
    public List<ProfileUpdates> getMatrixProfileUpdates() {
        return matrixProfileUpdates;
    }

    public void setMatrixProfileUpdates(final List<ProfileUpdates> matrixProfileUpdates) {
        this.matrixProfileUpdates = matrixProfileUpdates;
    }

    public void setResourceName(final String resourceName) {
        this.resourceName = resourceName;
    }
    
    public String getResourceName() {
        return resourceName;
    }
    
    public List<String> getResourceReferences() {
        return resourceReferences;
    }

    public void setResourceReferences(final List<String> resourceReferences) {
        this.resourceReferences = resourceReferences;
    }

    public void setResourceDescription(final String resourceDescription) {
        this.resourceDescription = resourceDescription;
    }
    
    public String getResourceDescription() {
        return resourceDescription;
    }
    
    public void setResourceDataLevel(final DataLevel resourceDataLevel) {
        this.resourceDataLevel = resourceDataLevel;
    }
    
    public DataLevel getResourceDataLevel() {
        return resourceDataLevel;
    }
    
    public List<Contact> getResourceContactNames() {
        return resourceContactNames;
    }

    public void setResourceContactNames(final List<Contact> resourceContactNames) {
        this.resourceContactNames = resourceContactNames;
    }

    public List<String> getResourceContactOrganisations() {
        return resourceContactOrganisations;
    }

    public void setResourceContactOrganisations(final List<String> resourceContactOrganisations) {
        this.resourceContactOrganisations = resourceContactOrganisations;
    }
}
