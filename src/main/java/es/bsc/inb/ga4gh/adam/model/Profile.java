/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.adam.model;

import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

public class Profile {

    private RestrictionsUL country;
    private List<ProfileDescription> allowedCountries;
    private RestrictionsUL organisation;
    private List<ProfileDescription> allowedOrganisations;
    private RestrictionsULF nonProfitOrganisation;
    private List<ProfileDescription> allowedNonProfitOrganisations;
    private RestrictionsULF profitOrganisation;
    private List<ProfileDescription> allowedProfitOrganisations;
    private RestrictionsUL person;
    private List<ProfileDescription> allowedPersons;
    private RestrictionsULF academicProfessional;
    private List<ProfileDescription> allowedAcademicProfessionals;
    private RestrictionsULF clinicalProfessional;
    private List<ProfileDescription> allowedClinicalProfessionals;
    private RestrictionsULF profitProfessional;
    private List<ProfileDescription> allowedProfitProfessionals;
    private RestrictionsULF nonProfessional;
    private List<ProfileDescription> allowedNonProfessionals;
    private RestrictionsULF nonProfitPurpose;
    private List<ProfileDescription> allowedNonProfitPurposes;
    private RestrictionsULF profitPurpose;
    private List<ProfileDescription> allowedProfitPurposes;
    private RestrictionsULF researchPurpose;
    private List<ProfileDescription> allowedResearchPurposes;
    private List<ProfileDescription> allowedResearchProfiles;
    private RestrictionsULF clinicalPurpose;
    private List<ProfileDescription> allowedClinicalPurpose;
    private List<ClinicalDescription> allowedClinicalProfiles;
    
    public RestrictionsUL getCountry() {
        return country;
    }

    public void setCountry(final RestrictionsUL country) {
        this.country = country;
    }
    
    public List<ProfileDescription> getAllowedCountries() {
        return allowedCountries;
    }

    public void setAllowedCountries(final List<ProfileDescription> allowedCountries) {
        this.allowedCountries = allowedCountries;
    }
    
    public RestrictionsUL getOrganisation() {
        return organisation;
    }

    public void setOrganisation(final RestrictionsUL organisation) {
        this.organisation = organisation;
    }

    public List<ProfileDescription> getAllowedOrganisations() {
        return allowedOrganisations;
    }

    public void setAllowedOrganisations(final List<ProfileDescription> allowedOrganisations) {
        this.allowedOrganisations = allowedOrganisations;
    }

    public RestrictionsULF getNonProfitOrganisation() {
        return nonProfitOrganisation;
    }

    public void setNonProfitOrganisation(final RestrictionsULF nonProfitOrganisation) {
        this.nonProfitOrganisation = nonProfitOrganisation;
    }

    public List<ProfileDescription> getAllowedNonProfitOrganisations() {
        return allowedNonProfitOrganisations;
    }

    public void setAllowedNonProfitOrganisations(final List<ProfileDescription> allowedNonProfitOrganisations) {
        this.allowedNonProfitOrganisations = allowedNonProfitOrganisations;
    }

    public RestrictionsULF getProfitOrganisation() {
        return profitOrganisation;
    }

    public void setProfitOrganisation(final RestrictionsULF profitOrganisation) {
        this.profitOrganisation = profitOrganisation;
    }

    public List<ProfileDescription> getAllowedProfitOrganisations() {
        return allowedProfitOrganisations;
    }

    public void setAllowedProfitOrganisations(final List<ProfileDescription> allowedProfitOrganisations) {
        this.allowedProfitOrganisations = allowedProfitOrganisations;
    }

    public RestrictionsUL getPerson() {
        return person;
    }

    public void setPerson(final RestrictionsUL person) {
        this.person = person;
    }

    public List<ProfileDescription> getAllowedPersons() {
        return allowedPersons;
    }

    public void setAllowedPersons(final List<ProfileDescription> allowedPersons) {
        this.allowedPersons = allowedPersons;
    }

    public RestrictionsULF getAcademicProfessional() {
        return academicProfessional;
    }

    public void setAcademicProfessional(final RestrictionsULF academicProfessional) {
        this.academicProfessional = academicProfessional;
    }
    
    public List<ProfileDescription> getAllowedAcademicProfessionals() {
        return allowedAcademicProfessionals;
    }

    public void setAllowedAcademicProfessionalsfinal(final List<ProfileDescription> allowedAcademicProfessionals) {
        this.allowedAcademicProfessionals = allowedAcademicProfessionals;
    }
    
    public RestrictionsULF getClinicalProfessional() {
        return clinicalProfessional;
    }

    public void setClinicalProfessional(final RestrictionsULF clinicalProfessional) {
        this.clinicalProfessional = clinicalProfessional;
    }

    public List<ProfileDescription> getAllowedClinicalProfessionals() {
        return allowedClinicalProfessionals;
    }

    public void setAllowedClinicalProfessionals(final List<ProfileDescription> allowedClinicalProfessionals) {
        this.allowedClinicalProfessionals = allowedClinicalProfessionals;
    }

    public RestrictionsULF getProfitProfessional() {
        return profitProfessional;
    }

    public void setProfitProfessional(final RestrictionsULF profitProfessional) {
        this.profitProfessional = profitProfessional;
    }

    public List<ProfileDescription> getAllowedProfitProfessionals() {
        return allowedProfitProfessionals;
    }

    public void setAllowedProfitProfessionals(final List<ProfileDescription> allowedProfitProfessionals) {
        this.allowedProfitProfessionals = allowedProfitProfessionals;
    }

    public RestrictionsULF getNonProfessional() {
        return nonProfessional;
    }

    public void setNonProfessional(final RestrictionsULF nonProfessional) {
        this.nonProfessional = nonProfessional;
    }
    
    public List<ProfileDescription> getAllowedNonProfessionals() {
        return allowedNonProfessionals;
    }

    public void setAllowedNonProfessionals(final List<ProfileDescription> allowedNonProfessionals) {
        this.allowedNonProfessionals = allowedNonProfessionals;
    }

    public RestrictionsULF getNonProfitPurpose() {
        return nonProfitPurpose;
    }

    public void setNonProfitPurpose(final RestrictionsULF nonProfitPurpose) {
        this.nonProfitPurpose = nonProfitPurpose;
    }

    public List<ProfileDescription> getAllowedNonProfitPurposes() {
        return allowedNonProfitPurposes;
    }

    public void setAllowedNonProfitPurposes(final List<ProfileDescription> allowedNonProfitPurposes) {
        this.allowedNonProfitPurposes = allowedNonProfitPurposes;
    }

    public RestrictionsULF getProfitPurpose() {
        return profitPurpose;
    }

    public void setProfitPurpose(final RestrictionsULF profitPurpose) {
        this.profitPurpose = profitPurpose;
    }

    public List<ProfileDescription> getAllowedProfitPurposes() {
        return allowedProfitPurposes;
    }

    public void setAllowedProfitPurposes(final List<ProfileDescription> allowedProfitPurposes) {
        this.allowedProfitPurposes = allowedProfitPurposes;
    }

    public RestrictionsULF getResearchPurpose() {
        return researchPurpose;
    }

    public void setResearchPurpose(final RestrictionsULF researchPurpose) {
        this.researchPurpose = researchPurpose;
    }

    public List<ProfileDescription> getAllowedResearchPurposes() {
        return allowedResearchPurposes;
    }

    public void setAllowedResearchPurposes(final List<ProfileDescription> allowedResearchPurposes) {
        this.allowedResearchPurposes = allowedResearchPurposes;
    }

    public List<ProfileDescription> getAllowedResearchProfiles() {
        return allowedResearchProfiles;
    }

    public void setAllowedResearchProfiles(final List<ProfileDescription> allowedResearchProfiles) {
        this.allowedResearchProfiles = allowedResearchProfiles;
    }

    public RestrictionsULF getClinicalPurpose() {
        return clinicalPurpose;
    }

    public void setClinicalPurpose(final RestrictionsULF clinicalPurpose) {
        this.clinicalPurpose = clinicalPurpose;
    }
    
    public List<ProfileDescription> getAllowedClinicalPurpose() {
        return allowedClinicalPurpose;
    }

    public void setAllowedClinicalPurpose(final List<ProfileDescription> allowedClinicalPurpose) {
        this.allowedClinicalPurpose = allowedClinicalPurpose;
    }
    
    public List<ClinicalDescription> getAllowedClinicalProfiles() {
        return allowedClinicalProfiles;
    }

    public void setAllowedClinicalProfiles(final List<ClinicalDescription> allowedClinicalProfiles) {
        this.allowedClinicalProfiles = allowedClinicalProfiles;
    }
}
