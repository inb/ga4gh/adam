/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.adam.model;

import java.util.List;

/**
 * @author Dmitry Repchevsky
 */

public class Terms {
   
    private Boolean noAuthorizationTerms;
    private List<String> whichAuthorizationTerms;
    private Boolean noPublicationTerms;
    private List<String> whichPublicationTerms;
    private Boolean noTimelineTerms;
    private List<String> whichTimelineTerms;
    private Boolean noSecurityTerms;
    private List<String> whichSecurityTerms;
    private Boolean noExpungingTerms;
    private List<String> whichExpungingTerms;
    private Boolean noLinkingTerms;
    private List<String> whichLinkingTerms;
    private Boolean noRecontactTerms;
    private List<String> allowedRecontactTerms;
    private List<String> compulsoryRecontactTerms;
    private Boolean noIPClaimTerms;
    private List<String> whichIPClaimTerms;
    private Boolean noReportingTerms;
    private List<String> whichReportingTerms;
    private Boolean noCollaborationTerms;
    private List<String> whichCollaborationTerms;
    private Boolean noPaymentTerms;
    private List<String> whichPaymentTerms;
    
    public Boolean getNoAuthorizationTerms() {
        return noAuthorizationTerms;
    }
    
    public void setNoAuthorizationTerms(final Boolean noAuthorizationTerms) {
        this.noAuthorizationTerms = noAuthorizationTerms;
    }
    
    public List<String> getWhichAuthorizationTerms() {
        return whichAuthorizationTerms;
    }

    public void setWhichAuthorizationTerms(final List<String> whichAuthorizationTerms) {
        this.whichAuthorizationTerms = whichAuthorizationTerms;
    }
    
    public Boolean getNoPublicationTerms() {
        return noPublicationTerms;
    }
    
    public void setNoPublicationTerms(final Boolean noPublicationTerms) {
        this.noPublicationTerms = noPublicationTerms;
    }
    
    public List<String> getWhichPublicationTerms() {
        return whichPublicationTerms;
    }

    public void setWhichPublicationTerms(final List<String> whichPublicationTerms) {
        this.whichPublicationTerms = whichPublicationTerms;
    }
    
    public Boolean getNoTimelineTerms() {
        return noTimelineTerms;
    }
    
    public void setNoTimelineTerms(final Boolean noTimelineTerms) {
        this.noTimelineTerms = noTimelineTerms;
    }

    public List<String> getWhichTimelineTerms() {
        return whichTimelineTerms;
    }

    public void setWhichTimelineTerms(final List<String> whichTimelineTerms) {
        this.whichTimelineTerms = whichTimelineTerms;
    }
    
    public Boolean getNoSecurityTerms() {
        return noSecurityTerms;
    }
    
    public void setNoSecurityTerms(final Boolean noSecurityTerms) {
        this.noSecurityTerms = noSecurityTerms;
    }
    
    public List<String> getWhichSecurityTerms() {
        return whichSecurityTerms;
    }

    public void setWhichSecurityTerms(final List<String> whichSecurityTerms) {
        this.whichSecurityTerms = whichSecurityTerms;
    }

    public Boolean getNoExpungingTerms() {
        return noExpungingTerms;
    }
    
    public void setNoExpungingTerms(final Boolean noExpungingTerms) {
        this.noExpungingTerms = noExpungingTerms;
    }

    public List<String> getWhichExpungingTerms() {
        return whichExpungingTerms;
    }

    public void setWhichExpungingTerms(final List<String> whichExpungingTerms) {
        this.whichExpungingTerms = whichExpungingTerms;
    }

    public Boolean getNoLinkingTerms() {
        return noLinkingTerms;
    }
    
    public void setNoLinkingTerms(final Boolean noLinkingTerms) {
        this.noLinkingTerms = noLinkingTerms;
    }

    public List<String> getWhichLinkingTerms() {
        return whichLinkingTerms;
    }

    public void setWhichLinkingTerms(final List<String> whichLinkingTerms) {
        this.whichLinkingTerms = whichLinkingTerms;
    }

    public Boolean getNoRecontactTerms() {
        return noRecontactTerms;
    }
    
    public void setNoRecontactTerms(final Boolean noRecontactTerms) {
        this.noRecontactTerms = noRecontactTerms;
    }

    public List<String> getAllowedRecontactTerms() {
        return allowedRecontactTerms;
    }

    public void setAllowedRecontactTerms(final List<String> allowedRecontactTerms) {
        this.allowedRecontactTerms = allowedRecontactTerms;
    }
    
    public List<String> getCompulsoryRecontactTerms() {
        return compulsoryRecontactTerms;
    }

    public void setCompulsoryRecontactTerms(final List<String> compulsoryRecontactTerms) {
        this.compulsoryRecontactTerms = compulsoryRecontactTerms;
    }
    
    public Boolean getNoIPClaimTerms() {
        return noIPClaimTerms;
    }
    
    public void setNoIPClaimTerms(final Boolean noIPClaimTerms) {
        this.noIPClaimTerms = noIPClaimTerms;
    }
    
    public List<String> getWhichIPClaimTerms() {
        return whichIPClaimTerms;
    }

    public void setWhichIPClaimTerms(final List<String> whichIPClaimTerms) {
        this.whichIPClaimTerms = whichIPClaimTerms;
    }

    public Boolean getNoReportingTerms() {
        return noReportingTerms;
    }
    
    public void setNoReportingTerms(final Boolean noReportingTerms) {
        this.noReportingTerms = noReportingTerms;
    }
    
    public List<String> getWhichReportingTerms() {
        return whichReportingTerms;
    }

    public void setWhichReportingTerms(final List<String> whichReportingTerms) {
        this.whichReportingTerms = whichReportingTerms;
    }

    public Boolean getNoCollaborationTerms() {
        return noCollaborationTerms;
    }
    
    public void setNoCollaborationTerms(final Boolean noCollaborationTerms) {
        this.noCollaborationTerms = noCollaborationTerms;
    }

    public List<String> getWhichCollaborationTerms() {
        return whichCollaborationTerms;
    }

    public void setWhichCollaborationTerms(final List<String> whichCollaborationTerms) {
        this.whichCollaborationTerms = whichCollaborationTerms;
    }

    public Boolean getNoPaymentTerms() {
        return noPaymentTerms;
    }
    
    public void setNoPaymentTerms(final Boolean noPaymentTerms) {
        this.noPaymentTerms = noPaymentTerms;
    }

    public List<String> getWhichPaymentTerms() {
        return whichPaymentTerms;
    }

    public void setWhichPaymentTerms(final List<String> whichPaymentTerms) {
        this.whichPaymentTerms = whichPaymentTerms;
    }
}
