These are Java Beans classes for the Automatable Discovery and Access Matrix (ADA-M) Json schema.

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/ga4gh/adam.git
>cd adam
>mvn install
```
The adam-1.0.jar will be generated in the adam/target/ directory

In most cases one would like to include ADA-M beans in the project.
It is easy to include depedency via maven (pom.xml):

```xml
<dependencies>
    <dependency>
        <groupId>es.bsc.inb.ga4gh</groupId>
        <artifactId>adam</artifactId>
        <version>1.0</version>
    </dependency>
</dependencies>     

<repositories>
    <repository>
        <id>adam</id>
        <url>https://gitlab.bsc.es/inb/ga4gh/adam/raw/maven</url>
    </repository>
</repositories>
```